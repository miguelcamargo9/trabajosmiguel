/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finalSimulacion.VO;

/**
 *
 * @author Miguel
 */
public class elementoVO {
  
  private Integer tapas;
  private Integer tapasCola;
  private Integer paquetes;
  private Integer paquetesCola;
  private Integer productoFinal;
  private Integer productoFinalCola;

  public elementoVO() {
  }

  public Integer getTapas() {
    return tapas;
  }

  public void setTapas(Integer tapas) {
    this.tapas = tapas;
  }

  public Integer getTapasCola() {
    return tapasCola;
  }

  public void setTapasCola(Integer tapasCola) {
    this.tapasCola = tapasCola;
  }

  public Integer getPaquetes() {
    return paquetes;
  }

  public void setPaquetes(Integer paquetes) {
    this.paquetes = paquetes;
  }

  public Integer getPaquetesCola() {
    return paquetesCola;
  }

  public void setPaquetesCola(Integer paquetesCola) {
    this.paquetesCola = paquetesCola;
  }

  public Integer getProductoFinal() {
    return productoFinal;
  }

  public void setProductoFinal(Integer productoFinal) {
    this.productoFinal = productoFinal;
  }

  public Integer getProductoFinalCola() {
    return productoFinalCola;
  }

  public void setProductoFinalCola(Integer productoFinalCola) {
    this.productoFinalCola = productoFinalCola;
  }
}

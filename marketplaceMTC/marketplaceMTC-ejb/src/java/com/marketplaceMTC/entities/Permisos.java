/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marketplaceMTC.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Miguel
 */
@Entity
@Table(name = "permisos")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "Permisos.findAll", query = "SELECT p FROM Permisos p"),
  @NamedQuery(name = "Permisos.findById", query = "SELECT p FROM Permisos p WHERE p.id = :id"),
  @NamedQuery(name = "Permisos.findByOpciones", query = "SELECT p FROM Permisos p WHERE p.opciones = :opciones")})
public class Permisos implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Integer id;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 50)
  @Column(name = "opciones")
  private String opciones;
  @JoinColumn(name = "idPerfil", referencedColumnName = "id")
  @ManyToOne(optional = false)
  private Perfiles idPerfil;

  public Permisos() {
  }

  public Permisos(Integer id) {
    this.id = id;
  }

  public Permisos(Integer id, String opciones) {
    this.id = id;
    this.opciones = opciones;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getOpciones() {
    return opciones;
  }

  public void setOpciones(String opciones) {
    this.opciones = opciones;
  }

  public Perfiles getIdPerfil() {
    return idPerfil;
  }

  public void setIdPerfil(Perfiles idPerfil) {
    this.idPerfil = idPerfil;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Permisos)) {
      return false;
    }
    Permisos other = (Permisos) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "com.marketplaceMTC.entities.Permisos[ id=" + id + " ]";
  }
  
}
